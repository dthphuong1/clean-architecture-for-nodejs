/**
 * Created by Phuong Duong on 08/02/2018
 */

var Idiom = require('./idiom')
    Account = require('./account')

exports.accountEntity = Account.accountEntity;
exports.idiomEntity = Idiom.idiomEntity;